package com.des.com.pocdesbacksigninsheet.dtos;


import java.util.ArrayList;
import java.util.List;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class TrainingDto {
	 String id;
	 String intitile;
	 List<TeacherEntity> teachers = new ArrayList<>();
	 List<StudentEntity> students = new ArrayList<>();
}
