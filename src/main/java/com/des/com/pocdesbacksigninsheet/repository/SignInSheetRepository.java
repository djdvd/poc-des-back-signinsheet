package com.des.com.pocdesbacksigninsheet.repository;

import com.des.com.pocdesbacksigninsheet.entities.SignInSheetEntity;

public interface SignInSheetRepository {
	
	public void createSignInSheet(SignInSheetEntity SignInSheet);
	public SignInSheetEntity readSignInSheet(Long idSignInSheet);
	public void updateSignInSheet(SignInSheetEntity SignInSheet);
	public void deleteSignInSheet(Long idSignInSheet);
	

}
