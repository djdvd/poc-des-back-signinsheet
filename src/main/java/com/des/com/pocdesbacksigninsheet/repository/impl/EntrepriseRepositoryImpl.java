package com.des.com.pocdesbacksigninsheet.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.des.com.pocdesbacksigninsheet.entities.EntrepriseEntity;
import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.repository.EntrepriseRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Repository
@Transactional
@FieldDefaults(level=AccessLevel.PRIVATE)
public class EntrepriseRepositoryImpl implements EntrepriseRepository {
	
	
	@Autowired
	EntityManager entityManager;

	@Override
	public void createEntreprise(EntrepriseEntity entreprise) {
		if (entreprise.getId() == null ) {
			//create
			entityManager.persist(entreprise);
		}
		updateEntreprise(entreprise);
		
	}

	@Override
	public EntrepriseEntity readEntreprise(Long idEntreprise) {
		TypedQuery<EntrepriseEntity> typeQueryEntreprise = entityManager.createQuery("Select e from EntrepriseEntity e  Where id = " + idEntreprise, EntrepriseEntity.class);
		return typeQueryEntreprise.getSingleResult();
	}

	@Override
	public void updateEntreprise(EntrepriseEntity entreprise) {
		if (entreprise.getId() != null ) entityManager.merge(entreprise);
		
	}

	@Override
	public void deleteEntreprise(Long idEntreprise) {
		entityManager.remove(entityManager.find(StudentEntity.class, idEntreprise));
		
	}

	
	
	

}
