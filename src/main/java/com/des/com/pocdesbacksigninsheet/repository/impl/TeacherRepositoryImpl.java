package com.des.com.pocdesbacksigninsheet.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;
import com.des.com.pocdesbacksigninsheet.repository.TeacherRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Repository
@Transactional
@FieldDefaults(level=AccessLevel.PRIVATE)
public class TeacherRepositoryImpl implements TeacherRepository {
	
	
	@Autowired
	EntityManager entityManager;

	@Override
	public void createTeacher(TeacherEntity Teacher) {
		if (Teacher.getId() == null ) {
			//create
			entityManager.persist(Teacher);
		}
		updateTeacher(Teacher);
	}

	@Override
	public TeacherEntity readTeacher(Long idTeacher) {
		TypedQuery<TeacherEntity> typeQueryTeacher = entityManager.createQuery("Select t from TeacherEntity t  Where id = " + idTeacher, TeacherEntity.class);
		return typeQueryTeacher.getSingleResult();
	}

	@Override
	public void updateTeacher(TeacherEntity teacher) {
		if (teacher.getId() != null ) entityManager.merge(teacher);
		
	}

	@Override
	public void deleteTeacher(Long idTeacher) {
		entityManager.remove(entityManager.find(TeacherEntity.class, idTeacher));
		
	}

	

}
