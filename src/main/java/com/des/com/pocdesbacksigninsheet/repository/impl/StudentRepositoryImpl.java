package com.des.com.pocdesbacksigninsheet.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Repository
@Transactional
@FieldDefaults(level=AccessLevel.PRIVATE)
public class StudentRepositoryImpl implements StudentRepository {
	
	
	@Autowired
	EntityManager entityManager;

	@Override
	public void createStudent(StudentEntity student) {
		if (student.getId() == null ) {
			//create
			entityManager.persist(student);
		}
		updateStudent(student);
		
	}

	@Override
	public StudentEntity readStudent(Long idStudent) {
		TypedQuery<StudentEntity> typeQueryStudent = entityManager.createQuery("Select s from StudentEntity s  Where id = " + idStudent, StudentEntity.class);
		return typeQueryStudent.getSingleResult();
		
	}

	@Override
	public void updateStudent(StudentEntity student) {
		if (student.getId() != null ) entityManager.merge(student);
	}

	@Override
	public void deleteStudent(Long idStudent) {
		entityManager.remove(entityManager.find(StudentEntity.class, idStudent));
	}

}
