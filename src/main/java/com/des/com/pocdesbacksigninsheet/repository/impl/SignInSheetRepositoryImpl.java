package com.des.com.pocdesbacksigninsheet.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.des.com.pocdesbacksigninsheet.entities.SignInSheetEntity;
import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.repository.SignInSheetRepository;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Repository
@Transactional
@FieldDefaults(level=AccessLevel.PRIVATE)
public class SignInSheetRepositoryImpl implements SignInSheetRepository {
	
	
	@Autowired
	EntityManager entityManager;

	@Override
	public void createSignInSheet(SignInSheetEntity signInSheet) {
		if (signInSheet.getId() == null ) {
			//create
			entityManager.persist(signInSheet);
		}
		updateSignInSheet(signInSheet);
		
	}

	@Override
	public SignInSheetEntity readSignInSheet(Long idSignInSheet) {
		TypedQuery<SignInSheetEntity> typeQuerySignInSheet = entityManager.createQuery("Select s from SignInSheetEntity s  Where id = " + idSignInSheet, SignInSheetEntity.class);
		return typeQuerySignInSheet.getSingleResult();
	}

	@Override
	public void updateSignInSheet(SignInSheetEntity signInSheet) {
		if (signInSheet.getId() != null ) entityManager.merge(signInSheet);
		
	}

	@Override
	public void deleteSignInSheet(Long idSignInSheet) {
		entityManager.remove(entityManager.find(SignInSheetEntity.class, idSignInSheet));
		
	}

	

	
	
	

}
