
package com.des.com.pocdesbacksigninsheet.repository;

import com.des.com.pocdesbacksigninsheet.entities.EntrepriseEntity;

public interface EntrepriseRepository {
	
	public void createEntreprise(EntrepriseEntity Entreprise);
	public EntrepriseEntity readEntreprise(Long idEntreprise);
	public void updateEntreprise(EntrepriseEntity Entreprise);
	public void deleteEntreprise(Long idEntreprise);
	

}
