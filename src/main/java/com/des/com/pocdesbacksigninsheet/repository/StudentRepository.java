package com.des.com.pocdesbacksigninsheet.repository;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;

public interface StudentRepository {
	
	public void createStudent(StudentEntity student);
	public StudentEntity readStudent(Long idStudent);
	public void updateStudent(StudentEntity student);
	public void deleteStudent(Long idStudent);
	

}
