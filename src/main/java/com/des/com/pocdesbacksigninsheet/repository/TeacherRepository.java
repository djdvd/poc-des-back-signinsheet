package com.des.com.pocdesbacksigninsheet.repository;

import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;

public interface TeacherRepository {
	
	public void createTeacher(TeacherEntity Teacher);
	public TeacherEntity readTeacher(Long idTeacher);
	public void updateTeacher(TeacherEntity Teacher);
	public void deleteTeacher(Long idTeacher);
	

}
