package com.des.com.pocdesbacksigninsheet.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.des.com.pocdesbacksigninsheet.entities.TrainingEntity;
import com.des.com.pocdesbacksigninsheet.repository.TrainingRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Repository
@Transactional
@FieldDefaults(level=AccessLevel.PRIVATE)
public class TrainingRepositoryImpl implements TrainingRepository {
	
	
	@Autowired
	EntityManager entityManager;

	@Override
	public void createTraining(TrainingEntity training) {
		if (training.getId() == null ) {
			//create
			entityManager.persist(training);
		}
		updateTraining(training);
		
	}

	@Override
	public TrainingEntity readTraining(Long idTraining) {
		TypedQuery<TrainingEntity> typeQueryTraining = entityManager.createQuery("Select t from TrainingEntity t  Where id = " + idTraining, TrainingEntity.class);
		return typeQueryTraining.getSingleResult();
	}

	@Override
	public void updateTraining(TrainingEntity training) {
		if (training.getId() != null ) entityManager.merge(training);
		
	}

	@Override
	public void deleteTraining(Long idTraining) {
		entityManager.remove(entityManager.find(TrainingEntity.class, idTraining));
		
	}

	
	

}
