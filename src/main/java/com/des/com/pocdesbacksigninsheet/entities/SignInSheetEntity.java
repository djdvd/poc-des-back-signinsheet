package com.des.com.pocdesbacksigninsheet.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.slf4j.Logger;

import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication;
import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication.PocDesBackSigninsheetApplicationBuilder;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
@Table(name="SIGNINSHEET")
public class SignInSheetEntity {
	@Id
	@GeneratedValue
	String id;
	String signature;
	String dateSignature;
	 
	@ManyToOne
	StudentEntity student;

}
