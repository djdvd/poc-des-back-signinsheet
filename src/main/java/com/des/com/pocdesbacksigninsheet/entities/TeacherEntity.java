package com.des.com.pocdesbacksigninsheet.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.slf4j.Logger;

import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication;
import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication.PocDesBackSigninsheetApplicationBuilder;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Entity
@Getter
@Setter
@FieldDefaults(level= AccessLevel.PRIVATE)
@Table(name="TEACHER")
public class TeacherEntity extends AbstractPersonal  {
	
	//Jointure table
	//actif responsible jointure
	@ManyToMany
	@JoinTable(
	    name = "TEACHER_TRAINING ", 
	    joinColumns = @JoinColumn(name = "TEACHER_ID"), 
	    inverseJoinColumns = @JoinColumn(name = "TRAINING_ID")
    )
	List<TrainingEntity> trainings = new ArrayList<>();
	
	public void addTrainingEntity(TrainingEntity training) {
		this.trainings.add(training);
	}

}
