package com.des.com.pocdesbacksigninsheet.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;

import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication;
import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication.PocDesBackSigninsheetApplicationBuilder;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name="STUDENT")
public class StudentEntity extends AbstractPersonal {
	

	@ManyToOne
	@NotNull
	EntrepriseEntity entreprise;
	
	@OneToMany(mappedBy="student")
	@JsonIgnore
	List<SignInSheetEntity> signInSheets;
	
	public void addSignInSheetEntity(SignInSheetEntity signInSheet) {
		this.signInSheets.add(signInSheet);
	}

	// actif responsible jointure
	@ManyToMany
	@JoinTable(name = "STUDENT_TRAINING ", joinColumns = @JoinColumn(name = "STUDENT_ID"), inverseJoinColumns = @JoinColumn(name = "TRAINING_ID"))
	List<TrainingEntity> trainings = new ArrayList<>();

	public void addTrainingEntity(TrainingEntity training) {
		this.trainings.add(training);
	}

}
