package com.des.com.pocdesbacksigninsheet.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;

import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication;
import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication.PocDesBackSigninsheetApplicationBuilder;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
@Table(name="TRAINING")
public class TrainingEntity {
	@Id
	@GeneratedValue
	 String id;
	 String intitile;
	  
	// we can have same training givein by two different personnes 
	 // training=java can have teached by  list of teacher = "David Esale and Herve C."
	 //passif responsible jointure
	@ManyToMany(mappedBy = "trainings")
	@JsonIgnore
	List<TeacherEntity> teachers = new ArrayList<>();
		
	public void addTeachers(TeacherEntity teacher) {
		this.teachers.add(teacher);
	}
	
	//we can have same training learning by two different personnes 
	// training java learning by a list of student : toto and titi
	 //passif responsible jointure
	@ManyToMany(mappedBy = "trainings")
	@JsonIgnore
	List<StudentEntity> students = new ArrayList<>();
	
	public void addStudents(StudentEntity student) {
		this.students.add(student);
	}

	@Override
	public String toString() {
		return String.format("training [%s]",intitile);
	}
	

}
