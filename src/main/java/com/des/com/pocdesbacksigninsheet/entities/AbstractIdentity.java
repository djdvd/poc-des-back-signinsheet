package com.des.com.pocdesbacksigninsheet.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.des.com.pocdesbacksigninsheet.enums.Category;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractIdentity {
	@Id
	@GeneratedValue
	protected String id;
	protected String name;
	protected Address Adresse;
	protected Category category;
	protected Contact contact;

}
