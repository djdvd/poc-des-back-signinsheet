package com.des.com.pocdesbacksigninsheet.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.slf4j.Logger;

import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication;
import com.des.com.pocdesbacksigninsheet.PocDesBackSigninsheetApplication.PocDesBackSigninsheetApplicationBuilder;
import com.des.com.pocdesbacksigninsheet.enums.Category;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
@Table(name="ENTREPRISE")
public class EntrepriseEntity extends AbstractIdentity  {
	
	@OneToMany(mappedBy="entreprise")
	List<StudentEntity> students = new ArrayList<>();
	
	public void addStudentEntity(StudentEntity student) {
		this.students.add(student);
	}
	

}
