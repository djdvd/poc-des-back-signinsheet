package com.des.com.pocdesbacksigninsheet.entities;

import java.util.List;

import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractPersonal extends AbstractIdentity {
	protected String firstName;
	protected String lastName;
}
