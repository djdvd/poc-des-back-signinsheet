package com.des.com.pocdesbacksigninsheet.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import com.des.com.pocdesbacksigninsheet.dtos.TrainingDto;
import com.des.com.pocdesbacksigninsheet.entities.TrainingEntity;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface TrainingMapper {

	TrainingDto  trainingEnityToTrainingDto(TrainingEntity training);
}
