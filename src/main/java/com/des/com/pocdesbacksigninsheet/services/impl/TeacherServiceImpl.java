package com.des.com.pocdesbacksigninsheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;
import com.des.com.pocdesbacksigninsheet.repository.TeacherRepository;
import com.des.com.pocdesbacksigninsheet.services.TeacherService;


@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;
	
	@Override
	public void createTeacher(TeacherEntity teacher) {
		teacherRepository.createTeacher(teacher);

	}

	@Override
	public TeacherEntity readTeacher(Long idTeacher) {
		// TODO Auto-generated method stub
		return teacherRepository.readTeacher(idTeacher);
	}

	@Override
	public void updateTeacher(TeacherEntity teacher) {
		teacherRepository.updateTeacher(teacher);

	}

	@Override
	public void deleteTeacher(Long idTeacher) {
		teacherRepository.deleteTeacher(idTeacher);

	}

}
