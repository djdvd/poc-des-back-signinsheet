package com.des.com.pocdesbacksigninsheet.services;

import com.des.com.pocdesbacksigninsheet.entities.TrainingEntity;

public interface TrainingService {
	public void createTraining(TrainingEntity training);
	public TrainingEntity readTraining(Long idStudent);
	public void updateTraining(TrainingEntity training);
	public void deleteTraining(Long idStudent);
}
