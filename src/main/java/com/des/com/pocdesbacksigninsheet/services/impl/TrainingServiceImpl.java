package com.des.com.pocdesbacksigninsheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.des.com.pocdesbacksigninsheet.entities.TrainingEntity;
import com.des.com.pocdesbacksigninsheet.repository.TrainingRepository;
import com.des.com.pocdesbacksigninsheet.services.TrainingService;


@Service
public class TrainingServiceImpl implements TrainingService {
	
	@Autowired
	private TrainingRepository trainingRepository;

	@Override
	public void createTraining(TrainingEntity training) {
		trainingRepository.createTraining(training);
		
	}

	@Override
	public TrainingEntity readTraining(Long idStudent) {
		return trainingRepository.readTraining(idStudent);
	}

	@Override
	public void updateTraining(TrainingEntity training) {
		trainingRepository.updateTraining(training);
		
	}

	@Override
	public void deleteTraining(Long idStudent) {
		trainingRepository.deleteTraining(idStudent);
		
	}

}
