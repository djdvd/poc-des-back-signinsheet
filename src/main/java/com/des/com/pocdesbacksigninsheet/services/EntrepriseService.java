package com.des.com.pocdesbacksigninsheet.services;

import com.des.com.pocdesbacksigninsheet.entities.EntrepriseEntity;

public interface EntrepriseService {
	public void createEntreprise(EntrepriseEntity Entreprise);
	public EntrepriseEntity readEntreprise(Long idEntreprise);
	public void updateEntreprise(EntrepriseEntity Entreprise);
	public void deleteEntreprise(Long idEntreprise);
}
