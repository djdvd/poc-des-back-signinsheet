package com.des.com.pocdesbacksigninsheet.services;

import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;

public interface TeacherService {
	public void createTeacher(TeacherEntity Teacher);
	public TeacherEntity readTeacher(Long idTeacher);
	public void updateTeacher(TeacherEntity Teacher);
	public void deleteTeacher(Long idTeacher);
}
