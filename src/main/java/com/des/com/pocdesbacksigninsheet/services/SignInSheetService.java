package com.des.com.pocdesbacksigninsheet.services;

import com.des.com.pocdesbacksigninsheet.entities.SignInSheetEntity;

public interface SignInSheetService {
	public void createSignInSheet(SignInSheetEntity SignInSheet);
	public SignInSheetEntity readSignInSheet(Long idSignInSheet);
	public void updateSignInSheet(SignInSheetEntity SignInSheet);
	public void deleteSignInSheet(Long idSignInSheet);
}
