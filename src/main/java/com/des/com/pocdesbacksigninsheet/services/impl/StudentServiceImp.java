package com.des.com.pocdesbacksigninsheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.services.StudentService;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

@Service
public class StudentServiceImp implements StudentService {
	
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public void createStudent(StudentEntity student) {
		studentRepository.createStudent(student);

	}

	@Override
	public StudentEntity readStudent(Long idStudent) {
		// TODO Auto-generated method stub
		return studentRepository.readStudent(idStudent);
	}

	@Override
	public void updateStudent(StudentEntity student) {
		studentRepository.updateStudent(student);

	}

	@Override
	public void deleteStudent(Long idStudent) {
		studentRepository.deleteStudent(idStudent);

	}

}
