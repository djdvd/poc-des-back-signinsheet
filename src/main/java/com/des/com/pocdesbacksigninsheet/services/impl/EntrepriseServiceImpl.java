package com.des.com.pocdesbacksigninsheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.des.com.pocdesbacksigninsheet.entities.EntrepriseEntity;
import com.des.com.pocdesbacksigninsheet.repository.EntrepriseRepository;
import com.des.com.pocdesbacksigninsheet.services.EntrepriseService;

@Service
public class EntrepriseServiceImpl implements EntrepriseService {
	
	@Autowired
	private EntrepriseRepository entrepriseRepository;

	@Override
	public void createEntreprise(EntrepriseEntity entreprise) {
		entrepriseRepository.createEntreprise(entreprise);

	}

	@Override
	public EntrepriseEntity readEntreprise(Long idEntreprise) {
		// TODO Auto-generated method stub
		return entrepriseRepository.readEntreprise(idEntreprise);
	}

	@Override
	public void updateEntreprise(EntrepriseEntity entreprise) {
		entrepriseRepository.updateEntreprise(entreprise);

	}

	@Override
	public void deleteEntreprise(Long idEntreprise) {
		entrepriseRepository.deleteEntreprise(idEntreprise);

	}

}
