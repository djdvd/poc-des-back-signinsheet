package com.des.com.pocdesbacksigninsheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.des.com.pocdesbacksigninsheet.entities.SignInSheetEntity;
import com.des.com.pocdesbacksigninsheet.repository.SignInSheetRepository;
import com.des.com.pocdesbacksigninsheet.services.SignInSheetService;


@Service
public class SignInSheetServiceImpl implements SignInSheetService {

	@Autowired
	private SignInSheetRepository signInSheetRepository;
	
	@Override
	public void createSignInSheet(SignInSheetEntity signInSheet) {
		signInSheetRepository.createSignInSheet(signInSheet);

	}

	@Override
	public SignInSheetEntity readSignInSheet(Long idSignInSheet) {
		// TODO Auto-generated method stub
		return signInSheetRepository.readSignInSheet(idSignInSheet);
	}

	@Override
	public void updateSignInSheet(SignInSheetEntity signInSheet) {
		signInSheetRepository.updateSignInSheet(signInSheet);

	}

	@Override
	public void deleteSignInSheet(Long idSignInSheet) {
		// TODO Auto-generated method stub

	}

}
