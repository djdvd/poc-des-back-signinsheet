package com.des.com.pocdesbacksigninsheet.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.services.StudentService;


@RestController
public class StudentController {
	private static final String PATH = "/student";
	@Autowired
	private StudentService studentService;
	
	@PostMapping(PATH)
	public void createStudent (@Valid @RequestBody StudentEntity studentEntity) {	
		studentService.createStudent(studentEntity);
	}
	
	@PutMapping(PATH)
	public void updateStudent(@RequestBody  StudentEntity studentEntity) {
		studentService.updateStudent(studentEntity);
	}
	
	@DeleteMapping(PATH + "/{id}")
	public void deleteStudent(@PathVariable("id") Long idStudent) {
		studentService.deleteStudent(idStudent);
	}
	
	@GetMapping(PATH + "/{id}")
	public StudentEntity retrieveStudent(@PathVariable("id") Long idStudent) {
		return studentService.readStudent(idStudent);		
	}
}
