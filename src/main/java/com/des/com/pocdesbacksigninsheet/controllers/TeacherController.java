package com.des.com.pocdesbacksigninsheet.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.des.com.pocdesbacksigninsheet.entities.TeacherEntity;
import com.des.com.pocdesbacksigninsheet.services.TeacherService;


@RestController
public class TeacherController {
	@Autowired
	private TeacherService teacherService;
	
	@PostMapping("/teacher")
	public void createTeacher (@Valid @RequestBody TeacherEntity teacherEntity) {	
		teacherService.createTeacher(teacherEntity);
	}
	
	@PutMapping("/teacher")
	public void updateTeacher(@RequestBody  TeacherEntity teacherEntity) {
		teacherService.updateTeacher(teacherEntity);
	}
	
	@DeleteMapping("/teacher/{id}")
	public void deleteTeacher(@PathVariable("id") Long idTeacher) {
		teacherService.deleteTeacher(idTeacher);
	}
	
	@GetMapping("/teacher/{id}")
	public TeacherEntity retrieveTeacher(@PathVariable("id") Long idTeacher) {
		return teacherService.readTeacher(idTeacher);		
	}
}
