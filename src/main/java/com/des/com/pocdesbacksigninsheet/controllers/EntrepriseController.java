package com.des.com.pocdesbacksigninsheet.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.des.com.pocdesbacksigninsheet.entities.EntrepriseEntity;
import com.des.com.pocdesbacksigninsheet.services.EntrepriseService;



@RestController
public class EntrepriseController {
	private static final String PATH = "/entreprise";
	@Autowired
	private EntrepriseService entrepriseService;
	
	@PostMapping(PATH)
	public void createEntreprise (@Valid @RequestBody EntrepriseEntity entrepriseEntity) {	
		entrepriseService.createEntreprise(entrepriseEntity);
	}
	
	@PutMapping(PATH)
	public void updateEntreprise(@RequestBody  EntrepriseEntity entrepriseEntity) {
		entrepriseService.updateEntreprise(entrepriseEntity);
	}
	
	@DeleteMapping(PATH + "/{id}")
	public void deleteEntreprise(@PathVariable("id") Long idEntreprise) {
		entrepriseService.deleteEntreprise(idEntreprise);
	}
	
	@GetMapping(PATH + "/{id}")
	public EntrepriseEntity retrieveEntreprise(@PathVariable("id") Long idEntreprise) {
		return entrepriseService.readEntreprise(idEntreprise);		
	}
}
