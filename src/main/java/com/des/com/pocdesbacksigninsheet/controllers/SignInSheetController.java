package com.des.com.pocdesbacksigninsheet.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.des.com.pocdesbacksigninsheet.entities.SignInSheetEntity;
import com.des.com.pocdesbacksigninsheet.services.SignInSheetService;


@RestController
public class SignInSheetController {
	private static final String PATH = "/signInSheet";
	@Autowired
	private SignInSheetService signInSheetService;
	
	@PostMapping(PATH)
	public void createSignInSheet (@Valid @RequestBody SignInSheetEntity signInSheetEntity) {	
		signInSheetService.createSignInSheet(signInSheetEntity);
	}
	
	@PutMapping(PATH)
	public void updateSignInSheet(@RequestBody  SignInSheetEntity signInSheetEntity) {
		signInSheetService.updateSignInSheet(signInSheetEntity);
	}
	
	@DeleteMapping(PATH + "/{id}")
	public void deleteSignInSheet(@PathVariable("id") Long idSignInSheet) {
		signInSheetService.deleteSignInSheet(idSignInSheet);
	}
	
	@GetMapping(PATH + "/{id}")
	public SignInSheetEntity retrieveSignInSheet(@PathVariable("id") Long idSignInSheet) {
		return signInSheetService.readSignInSheet(idSignInSheet);		
	}
}
