package com.des.com.pocdesbacksigninsheet.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.des.com.pocdesbacksigninsheet.entities.TrainingEntity;
import com.des.com.pocdesbacksigninsheet.services.TrainingService;

@RestController
public class TrainingController {
	
	@Autowired
	private TrainingService trainingService;
	
	@PostMapping("/training")
	public void createTraining (@Valid @RequestBody TrainingEntity trainingEntity) {	
		trainingService.createTraining(trainingEntity);
	}
	
	@PutMapping("/training")
	public void updateTraining(@RequestBody  TrainingEntity trainingEntity) {
		trainingService.updateTraining(trainingEntity);
	}
	
	@DeleteMapping("/training/{id}")
	public void deleteTraining(@PathVariable("id") Long idTraining) {
		trainingService.deleteTraining(idTraining);
	}
	
	@GetMapping("/training/{id}")
	public TrainingEntity retrieveTraining(@PathVariable("id") Long idTraining) {
		return trainingService.readTraining(idTraining);		
	}

}
