package com.des.com.pocdesbacksigninsheet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.des.com.pocdesbacksigninsheet.entities.StudentEntity;
import com.des.com.pocdesbacksigninsheet.repository.StudentRepository;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Builder
@SpringBootApplication
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class PocDesBackSigninsheetApplication /* implements CommandLineRunner */ {

	//final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	StudentRepository studentRepository;

	public static void main(String[] args) {
		SpringApplication.run(PocDesBackSigninsheetApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		
//		StudentEntity studentEntity = new StudentEntity();
//		studentEntity.setFirstName("David");
//		studentEntity.setLastName("Esale Yoka");
//		studentRepository.createStudent(studentEntity);
//		log.info("save {} " + studentEntity);
//		
//	}

}
